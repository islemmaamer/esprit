package com.alibou.keycloak.services;

import com.alibou.keycloak.entities.GroceryItem;

public interface GroceryItemService {

    public GroceryItem addItem(GroceryItem groceryItem);
}

package com.alibou.keycloak.services;

import com.alibou.keycloak.entities.GroceryItem;
import com.alibou.keycloak.repositories.ItemRepository;
import com.alibou.keycloak.services.GroceryItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

@Service
public class GroceryItemImpl implements GroceryItemService {

    @Autowired
    private  ItemRepository itemRepository;


    @Override
    public GroceryItem addItem(GroceryItem groceryItem) {
        return itemRepository.save(groceryItem);
    }
}

package com.alibou.keycloak.services.keycloak.dto;

import com.alibou.keycloak.entities.Role;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CreateKeycloakUserRequest {
    String username;
    String password;
    String email;
    String firstname;
    String lastname;
    Role role;
}



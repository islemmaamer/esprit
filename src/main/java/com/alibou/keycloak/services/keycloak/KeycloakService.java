package com.alibou.keycloak.services.keycloak;


import com.alibou.keycloak.services.keycloak.dto.CreateKeycloakUserRequest;
import com.alibou.keycloak.services.keycloak.dto.LoginKeycloakResponse;

public interface KeycloakService {

    LoginKeycloakResponse login(String username, String password);

    String createUser(CreateKeycloakUserRequest createKeycloakUserRequest);

}

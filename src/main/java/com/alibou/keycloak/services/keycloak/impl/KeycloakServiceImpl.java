package com.alibou.keycloak.services.keycloak.impl;

import com.alibou.keycloak.entities.Role;
import com.alibou.keycloak.services.keycloak.KeycloakProvider;
import com.alibou.keycloak.services.keycloak.KeycloakServer;
import com.alibou.keycloak.services.keycloak.KeycloakService;
import com.alibou.keycloak.services.keycloak.dto.CreateKeycloakUserRequest;
import com.alibou.keycloak.services.keycloak.dto.LoginKeycloakRequest;
import com.alibou.keycloak.services.keycloak.dto.LoginKeycloakResponse;
import org.keycloak.admin.client.CreatedResponseUtil;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.Response;
import java.util.Collections;

@Service
public class KeycloakServiceImpl implements KeycloakService {

    @Value("${keycloak.client-id}")
    public String clientId;

    @Value("${keycloak.realm}")
    private String realm;

    private final KeycloakProvider keycloakProvider;

    private final KeycloakServer keycloakServer;

    public KeycloakServiceImpl(KeycloakProvider keycloakProvider, KeycloakServer keycloakServer) {
        this.keycloakProvider = keycloakProvider;
        this.keycloakServer = keycloakServer;
    }

    @Override
    public LoginKeycloakResponse login(String username, String password) {
        LoginKeycloakRequest loginKeycloakRequest = LoginKeycloakRequest.builder()
                .username(username)
                .password(password)
                .client_id(clientId)
                .grant_type("password").build();
        return keycloakServer.authenticate(realm, loginKeycloakRequest);
    }

    @Override
    public String createUser(CreateKeycloakUserRequest createKeycloakUserRequest) {
        Keycloak keycloak = keycloakProvider.getInstance();
        RealmResource realmsResource = keycloak.realm(realm);
        UsersResource usersResource = realmsResource.users();

        CredentialRepresentation credentialRepresentation = createPasswordCredentials(createKeycloakUserRequest.getPassword());
        RoleRepresentation realmRole = realmsResource.roles().get(createKeycloakUserRequest.getRole().toString()).toRepresentation();

        UserRepresentation kcUser = new UserRepresentation();
        kcUser.setUsername(createKeycloakUserRequest.getUsername());
        kcUser.setCredentials(Collections.singletonList(credentialRepresentation));
        kcUser.setFirstName(createKeycloakUserRequest.getFirstname());
        kcUser.setLastName(createKeycloakUserRequest.getLastname());
        kcUser.setEmail(createKeycloakUserRequest.getEmail());
        kcUser.setEnabled(true);
        kcUser.setEmailVerified(true);
        Response response = usersResource.create(kcUser);
        String userId = CreatedResponseUtil.getCreatedId(response);

        /*
        RoleRepresentation roleAdmin = realmsResource.roles().get(Role.ADMIN.toString()).toRepresentation();

         */
        usersResource.get(userId).roles().realmLevel().add(Collections.singletonList(realmRole));

        return userId;
    }

    private CredentialRepresentation createPasswordCredentials(String password) {
        CredentialRepresentation passwordCredentials = new CredentialRepresentation();
        passwordCredentials.setTemporary(false);
        passwordCredentials.setType(CredentialRepresentation.PASSWORD);
        passwordCredentials.setValue(password);
        return passwordCredentials;
    }


}

package com.alibou.keycloak.services.keycloak;


import com.alibou.keycloak.config.FormFeignEncoderConfig;
import com.alibou.keycloak.services.keycloak.dto.LoginKeycloakRequest;
import com.alibou.keycloak.services.keycloak.dto.LoginKeycloakResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


@FeignClient(name = "KeycloakServer", url = "${keycloak.auth-server-url}", configuration = FormFeignEncoderConfig.class)
public interface KeycloakServer {
    @PostMapping(value = "/realms/{realm}/protocol/openid-connect/token", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    LoginKeycloakResponse authenticate(@PathVariable("realm") String realm, @RequestBody LoginKeycloakRequest loginKeycloakRequest);
}
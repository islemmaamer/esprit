package com.alibou.keycloak.services.keycloak.dto;

import lombok.Data;

@Data
public class LoginKeycloakResponse {
    private String access_token;
    private int expires_in;
    private int refresh_expires_in;
    private String refresh_token;
    private String token_type;
}

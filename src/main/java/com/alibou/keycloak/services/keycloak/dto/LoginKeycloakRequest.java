package com.alibou.keycloak.services.keycloak.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class LoginKeycloakRequest {

    String username;
    String password;
    String client_id ;
    String grant_type;
}

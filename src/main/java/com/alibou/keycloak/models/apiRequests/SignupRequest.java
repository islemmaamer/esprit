package com.alibou.keycloak.models.apiRequests;

import lombok.Builder;
import lombok.Data;
@Data
@Builder
public class SignupRequest {
    String username;
    String password;
    String email;
    String firstname;
    String lastname;
}

package com.alibou.keycloak.models.apiRequests;

import lombok.Builder;
import lombok.Data;
@Data
@Builder
public class LoginRequest {
    String username;
    String password;
}

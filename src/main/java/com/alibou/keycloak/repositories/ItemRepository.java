package com.alibou.keycloak.repositories;

import com.alibou.keycloak.entities.GroceryItem;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ItemRepository extends MongoRepository<GroceryItem, String> {

}
package com.alibou.keycloak.config;

import com.alibou.keycloak.security.JwtAuthConverter;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;

import static org.springframework.security.config.http.SessionCreationPolicy.STATELESS;

@Configuration
@EnableWebSecurity
@EnableMethodSecurity
@RequiredArgsConstructor
public class SecurityConfig {

    private final JwtAuthConverter jwtAuthConverter;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http
                .csrf()
                    .disable()
                .authorizeHttpRequests()
                .requestMatchers("/login","/signup","/add","/produit/add-produit","/produit/retrieve-all-produits"
                        ,"/produit/retrieve-produit/{produit-id}","/produit/remove-produit/{produit-id}","/stock/add-stock","/livraison/add-livraison","/stock/retrieve-all-stocks","/livraison/retrieve-all-livraisons," +
                                "/stock/retrieve-stock/{stock-id}","/livraison/retrieve-livraison/{livraison-id}","/stock/remove-stock/{stock-id}",
                        "/livraison/remove-livraison/{livraison-id}")
                .permitAll()
                    .anyRequest()
                        .authenticated();
        //permitAll()

        http
                .oauth2ResourceServer()
                    .jwt()
                        .jwtAuthenticationConverter(jwtAuthConverter);

        http
                .sessionManagement()
                    .sessionCreationPolicy(STATELESS);

        return http.build();
    }
}

package com.alibou.keycloak.controllers;

import com.alibou.keycloak.entities.GroceryItem;
import com.alibou.keycloak.entities.Role;
import com.alibou.keycloak.models.apiRequests.LoginRequest;
import com.alibou.keycloak.models.apiRequests.SignupRequest;
import com.alibou.keycloak.services.keycloak.KeycloakService;
import com.alibou.keycloak.services.keycloak.dto.CreateKeycloakUserRequest;
import com.alibou.keycloak.services.keycloak.dto.LoginKeycloakResponse;
import com.alibou.keycloak.services.GroceryItemService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
public class DemoController {

    private final KeycloakService keycloakService;

    private final GroceryItemService groceryItemService;

    public DemoController(KeycloakService keycloakService, GroceryItemService groceryItemService) {
        this.keycloakService = keycloakService;
        this.groceryItemService = groceryItemService;
    }

    @GetMapping
    @PreAuthorize("hasRole('USER')")
    public String hello() {
        return "Hello from Spring boot & Keycloak";
    }

    @PostMapping("/login")
    public LoginKeycloakResponse login(@RequestBody LoginRequest loginRequest) {
        return keycloakService.login(loginRequest.getUsername(), loginRequest.getPassword());
    }

    @PostMapping("/signup")
    public String signup(@RequestBody SignupRequest signupRequest) {
        CreateKeycloakUserRequest createKeycloakUserRequest =
                CreateKeycloakUserRequest.builder()
                        .role(Role.ADMIN)
                        .firstname(signupRequest.getFirstname())
                        .lastname(signupRequest.getLastname())
                        .email(signupRequest.getEmail())
                        .username(signupRequest.getUsername())
                        .password(signupRequest.getPassword())
                        .build();
        return keycloakService.createUser(createKeycloakUserRequest);
    }

    @PostMapping("/add")
    public GroceryItem add(@RequestBody GroceryItem groceryItem) {
        return groceryItemService.addItem(groceryItem);
    }

}

# Utilisez l'image OpenJDK 17 comme base
FROM openjdk:17

# Exposez le port 8089 (si nécessaire)
EXPOSE 8089

# Copiez le fichier JAR (assurez-vous que le nom du fichier est correct)
COPY target/keycloak-0.0.1-SNAPSHOT.jar keycloak.jar

# Définissez la commande d'entrée
ENTRYPOINT ["java", "-jar", "/keycloak.jar"]